<?php

namespace Favez\Mvc\Event;

class Manager
{
    
    protected $listeners = [];
    
    public function subscribe($name, $callable, $priority = 0)
    {
        if (is_callable($callable))
        {
            if (!isset($this->listeners[$name]))
            {
                $this->listeners[$name] = [];
            }
            
            $this->listeners[$name][$priority][] = $callable;
            
            ksort($this->listeners[$name], SORT_ASC);
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Collects data from event listeners.
     *
     * @param string                           $name
     * @param array|\Favez\Mvc\Event\Arguments $arguments
     *
     * @return array
     */
    public function collect($name, $arguments = [])
    {
        $items = [];
        
        if (!($arguments instanceof Arguments))
        {
            $arguments = new Arguments($arguments);
        }
    
        if ($listeners = $this->getListeners($name))
        {
            foreach ($listeners as $listenerContainer)
            {
                foreach ($listenerContainer as $listener)
                {
                    $item = call_user_func($listener, $arguments);
                    
                    if (is_array($item) === true)
                    {
                        $items = array_merge_recursive($items, $item);
                    }
                
                    if ($arguments->processed())
                    {
                        break;
                    }
                }
            }
        }
    
        return $items;
    }
    
    /**
     * @param string          $name
     * @param array|Arguments $arguments
     * @return mixed
     */
    public function publish($name, $arguments = [])
    {
        if (!($arguments instanceof Arguments))
        {
            $arguments = new Arguments($arguments);
        }
        
        if ($listeners = $this->getListeners($name))
        {
            foreach ($listeners as $listenerContainer)
            {
                foreach ($listenerContainer as $listener)
                {
                    call_user_func($listener, $arguments);
                    
                    if ($arguments->processed())
                    {
                        break;
                    }
                }
            }
        }
        
        return $arguments->getReturn();
    }
    
    public function hasListeners($name)
    {
        return isset($this->listeners[$name]) && !empty($this->listeners[$name]);
    }
    
    public function getListeners($name)
    {
        if (!$this->hasListeners($name))
        {
            return null;
        }
        
        return $this->listeners[$name];
    }
    
    public function getAllListeners()
    {
        return $this->listeners;
    }
    
    /**
     * @param string|null $name
     * @return array|null
     * @deprecated
     */
    public function listeners($name = null)
    {
        trigger_error(__CLASS__ . '::listeners() is deprecated. Use hasListeners, getListeners or getAllListeners instead.');
        
        if (empty($name))
        {
            return $this->getAllListeners();
        }
        elseif ($this->hasListeners($name))
        {
            return $this->getListeners($name);
        }
        
        return null;
    }
    
}