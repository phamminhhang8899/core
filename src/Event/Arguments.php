<?php

namespace Favez\Mvc\Event;

use Favez\Mvc\Container;

class Arguments extends Container implements \Countable, \ArrayAccess
{

    /**
     * @var mixed
     */
    protected $return;

    /**
     * @var boolean
     */
    protected $processed;

    public function __construct($data)
    {
        parent::__construct($data);

        $this->processed = false;
    }

    public function subject()
    {
        return $this->get('subject');
    }

    public function setReturn($return)
    {
        $this->return = $return;
    }

    public function getReturn()
    {
        return $this->return;
    }

    public function processed()
    {
        return $this->processed;
    }

    public function stop()
    {
        $this->processed = true;
    }

    public function count()
    {
        return count($this->data);
    }

}