<?php

namespace Favez\Mvc\Hook;

use Favez\Mvc\App;
use Favez\Mvc\Event\Arguments;
use Favez\Mvc\Exception\AccessViolationException;

class Proxy
{
    
    protected $className;
    
    protected $subject;
    
    protected $reflection;
    
    public function __construct($subject)
    {
        $this->className  = get_class($subject);
        $this->subject    = $subject;
        $this->reflection = new \ReflectionClass($subject);
    }
    
    public function __call($name, $arguments = [])
    {
        $reflectionMethod = $this->reflection->getMethod($name);
        $reflectionParams = $reflectionMethod->getParameters();
        
        $params = [];
        $index  = 0;
        
        foreach ($reflectionParams as $reflectionParam)
        {
            $params[$reflectionParam->name] = $arguments[$index++];
        }
        
        App::events()->publish($this->className . '.' . $name . '.before', $params);
        
        $result = $reflectionMethod->invoke($this->subject, $arguments);

        $args   = new Arguments($params);
        $args->setReturn($result);
        
        App::events()->publish($this->className . '.' . $name . '.after', $args);
        
        return $args->getReturn();
    }
    
    public function __get($name)
    {
        $reflectionProperty = $this->reflection->getProperty($name);
    
        if ($reflectionProperty->isPublic())
        {
            $reflectionProperty->getValue($this->subject);
        }
        
        throw new AccessViolationException($name);
    }
    
    public function __set($name, $value)
    {
        $reflectionProperty = $this->reflection->getProperty($name);
        
        if ($reflectionProperty->isPublic())
        {
            $reflectionProperty->setValue($this->subject, $value);
        }
        
        throw new AccessViolationException($name);
    }
    
}