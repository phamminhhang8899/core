<?php

namespace Favez\Mvc;

use Favez\Mvc\DI\Injectable;

/**
 * Class Modules
 *
 * @package Favez\Mvc
 *
 * @method static Modules instance()
 */
class Modules
{
    use Singleton, Injectable;
}