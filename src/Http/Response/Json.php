<?php

namespace Favez\Mvc\Http\Response;

use Favez\Mvc\App;
use Favez\Mvc\Singleton;
use Slim\Http\Response;

class Json
{
    use Singleton;
    
    private $data;

    public function __construct()
    {
        $this->data = [];
    }
    
    public function assign($name, $value = null)
    {
        if (is_array($name))
        {
            foreach ($name as $key => $value)
            {
                $this->data[$key] = $value;
            }
        }
        else if(is_string($name))
        {
            $this->data[$name] = $value;
        }
        else if(is_object($name))
        {
            $this->data = (array) $name;
        }
        
        return $this;
    }
    
    /**
     * @deprecated Use Json::assign instead.
     *
     * @param array|string $name
     * @param mixed        $value
     *
     * @return $this
     */
    public function add($name, $value = null)
    {
        return $this->assign($name, $value);
    }
    
    public function has($key)
    {
        return isset($this->data[$key]);
    }

    public function remove($key)
    {
        if ($this->has($key))
        {
            unset($this->data[$key]);
        }

        return $this;
    }

    public function success($data = [])
    {
        $this->assign($data);
        $this->assign('success', true);

        return $this->send();
    }

    public function failure($data = [])
    {
        $this->assign($data);
        $this->assign('success', false);

        return $this->send();
    }

    public function send()
    {
        return (new Response())
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(
                json_encode(
                    $this->data
                )
            );
    }
}