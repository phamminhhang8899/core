<?php

namespace Favez\Mvc\Http;

class Cookies implements CookiesInterface
{

    public function set($key, $value, $expire = null, $path = null, $domain = null, $secure = null, $httpOnly = null)
    {
        return setcookie($key, $value, $expire, $path, $domain, $secure, $httpOnly);
    }

    public function get($key, $default = null)
    {
        if (isset($_COOKIE[$key]))
        {
            return $_COOKIE[$key];
        }

        return $default;
    }

    public function reset($key)
    {
        $this->set($key, '', 0);
    }

}