<?php

namespace Favez\Mvc;

use Composer\Autoload\ClassLoader;
use Favez\Mvc\DI\Injectable;
use Favez\Mvc\Exception\InvalidTargetException;
use Favez\Mvc\Http\Cookies;
use Favez\ORM\EntityManager;

/**
 * Class App
 *
 * @category PHP_Framework
 * @package  Favez\Mvc
 * @author   tyurderi <tyurderi@yahoo.de>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     http://github.com/favez/favez
 *
 * @method static App instance(array $container = [])
 */
class App extends \Slim\App
{
    use Singleton;
    use Injectable;
    
    private static $_di;
    
    /**
     * The composer autoload class.
     * It can be null if it was not passed to the application by self::setLoader.
     *
     * @var ClassLoader
     */
    private $_loader;

    /**
     * A helper class to get dependency injection modules.
     *
     * @deprecated Use static module access through this class instead.
     * @return     Modules
     */
    public static function modules()
    {
        return Modules::instance();
    }
    
    /**
     * Gets the dependency injection container.
     *
     * @return DI\Container
     */
    public static function di()
    {
        if (self::$_di === null) {
            self::$_di = new DI\Container();
        }
        
        return self::$_di;
    }
    
    /**
     * App constructor.
     *
     * @param array $container Basic container with app configuration.
     */
    public function __construct($container = [])
    {
        parent::__construct($container);
        
        // Initialize session
        $this->add(new \Slim\Middleware\Session([
            'name'        => $this->config('cookieName', 'favez_session'),
            'autorefresh' => true,
            'lifetime'    => '1 hour'
        ]));

        $this->initDIContainer();

        self::subscriber()->register();
        self::events()->publish('core.app.init');
    }
    
    /**
     * Get a configuration item by array key path.
     *
     * @param string $key     The key of the config item.
     * @param mixed  $default Default value if no config item found.
     *
     * @return mixed
     */
    public function config($key, $default = null)
    {
        return fetch_array($key, $default, $this->getContainer()->get('config'));
    }
    
    /**
     * Run the app.
     *
     * @param bool $silent {inheritdoc}
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function run($silent = false)
    {
        self::events()->publish('core.app.run');

        return parent::run($silent);
    }
    
    /**
     * Route to target mapping.
     *
     * @param array           $methods Allowed HTTP methods.
     * @param string          $pattern The route pattern.
     * @param callable|string $target  The target that should be called.
     *
     * @return \Slim\Interfaces\RouteInterface
     * @throws \Favez\Mvc\Exception\InvalidTargetException
     */
    public function map(array $methods, $pattern, $target)
    {
        if (is_string($target) || is_array($target)) {
            return parent::map(
                $methods,
                $pattern,
                function ($request, $response, $params) use ($target) {
                    return self::dispatcher()->dispatch(
                        $target,
                        $params
                    );
                }
            );
        } else if (is_callable($target)) {
            return parent::map($methods, $pattern, $target);
        } else {
            throw new InvalidTargetException();
        }
    }
    
    /**
     * Whether the app is in debug mode or not. Usually used for development.
     *
     * @return bool
     */
    public function isDebug()
    {
        return $this->config('debug') === true;
    }
    
    /**
     * Whether the app is in test mode or not. Usually used by phpunit.
     *
     * @return bool
     */
    public function isTest()
    {
        return $this->config('test') === true;
    }

    /**
     * Get the correct url by path.
     *
     * @param string $path The path after the url.
     *
     * @return mixed
     *
     * @deprecated Use UrlService::get or Injectable::url instead
     */
    public function url($path = '')
    {
        return self::urlService()->get($path);
    }
    
    /**
     * Initialize all default dependency container items.
     *
     * @return void
     */
    protected function initDIContainer()
    {
        self::di()->registerShared(
            [
                'path' => function () {
                    return $this->config('app.path');
                },
                'cache' => function () {
                    return new \Stash\Pool(
                        new \Stash\Driver\FileSystem(
                            [
                                'path' => $this->config('app.path') . $this->config('app.cache_path'),
                                'filePermissions' => 0777,
                                'dirPermissions'  => 0777
                            ]
                        )
                    );
                },
                'db' => function () {
                    return new Database\Database($this);
                },
                'view' => function () {
                    $view = new View\View();
    
                    if ($this->isDebug()) {
                        $view->engine()->enableDebug();
                        $view->engine()->addExtension(new \Twig_Extension_Debug());
                    }
    
                    $view->engine()->addExtension(new View\Extension($this, $view));
    
                    return $view;
                },
                'models' => function () {
                    return new \Favez\ORM\App(self::db()->pdo());
                },
                'json' => function () {
                    return new Http\Response\Json();
                },
                'events' => function () {
                    return new Event\Manager();
                },
                'subscriber' => function () {
                    return new Subscriber\Manager($this);
                },
                'dispatcher' => function () {
                    return new Dispatcher();
                },
                'session' => function () {
                    return new \SlimSession\Helper();
                },
                'cookies' => function () {
                    return new Cookies();
                },
                'urlService' => function () {
                    return new UrlService(self::request());
                }
            ]
        );

        self::di()->register(
            [
                'app' => function () {
                    return $this;
                },
                'request' => function () {
                    return $this->getContainer()->get('request');
                },
                'response' => function () {
                    return $this->getContainer()->get('response');
                },
                'config' => function ($key, $default = null) {
                    return $this->config($key, $default);
                },
                'url' => function ($path = '') {
                    return self::urlService()->get($path);
                }
            ]
        );
    }
    
    /**
     * Set the app autoloader from composer.
     *
     * @param \Composer\Autoload\ClassLoader $_loader The loader instance.
     *
     * @return void
     */
    public function setLoader($_loader)
    {
        $this->_loader = $_loader;
    }
    
    /**
     * Get the apps composer autoloader.
     *
     * @return \Composer\Autoload\ClassLoader
     */
    public function loader()
    {
        return $this->_loader;
    }

}