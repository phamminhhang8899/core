<?php

namespace Favez\Mvc;

trait Singleton
{

    protected static $instance = null;

    public static function instance()
    {
        if(self::$instance === null)
        {
            $reflectionClass = new \ReflectionClass(__CLASS__);
            self::$instance  = $reflectionClass->newInstanceArgs(func_get_args());
        }

        return self::$instance;
    }

}