<?php

namespace Favez\Mvc\ORM;

use Favez\Mvc\App;

abstract class Entity extends \Favez\ORM\Entity\Entity
{

    public static function repository ()
    {
        return App::models()->getRepository(static::class);
    }

    public function save ()
    {
        return self::repository()->save($this);
    }

    public static function create ()
    {
        return self::repository()->create();
    }

    /**
     * @deprecated Use remove() instead
     */
    public function delete()
    {
        $this->remove();
    }

    public function remove ()
    {
        return self::repository()->remove($this);
    }

}
