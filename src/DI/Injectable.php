<?php

namespace Favez\Mvc\DI;

use Favez\Mvc\App;

/**
 * Trait Injectable
 *
 * @package Favez\Mvc\DI
 *
 * @method static string                        path()
 * @method static \Stash\Pool                   cache()
 * @method static \Favez\Mvc\Database\Database  db()
 * @method static \Favez\Mvc\View\View          view(array $configuration = [])
 * @method static \Favez\ORM\App                models()
 * @method static \Favez\Mvc\Http\Response\Json json()
 * @method static \Favez\Mvc\Event\Manager      events()
 * @method static \Favez\Mvc\Subscriber\Manager subscriber()
 * @method static \Favez\Mvc\Dispatcher         dispatcher()
 * @method static \SlimSession\Helper           session()
 * @method static \Favez\Mvc\Http\Cookies       cookies()
 * @method static \Favez\Mvc\UrlService         urlService()
 *
 * @method static mixed  config($key, $default = null)
 * @method static string url($path = '')
 *
 * @method static \Favez\Mvc\App       app()
 * @method static \Slim\Http\Request   request()
 * @method static \Slim\Http\Response  response()
 */
trait Injectable
{

    public function __call($name, $arguments)
    {
        return App::di()->get($name, $arguments);
    }

    public static function __callStatic($name, $arguments)
    {
        return App::di()->get($name, $arguments);
    }

}