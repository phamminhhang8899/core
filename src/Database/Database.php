<?php

namespace Favez\Mvc\Database;

use Favez\Mvc\DI\Injectable;

class Database
{
    use Injectable;

    /**
     * @var \FluentPDO
     */
    protected $db;
    
    /**
     * @var \PDO
     */
    protected $pdo;

    public function __construct(\Favez\Mvc\App $app)
    {
        $this->pdo = new \PDO(
            sprintf('mysql:host=%s;dbname=%s',
                $app->config('database.host'),
                $app->config('database.shem')
            ),
            $app->config('database.user'),
            $app->config('database.pass')
        );

        $this->db = new \FluentPDO($this->pdo);

        $this->events()->publish('core.database.init', [$this]);
        
        if ($this->app()->isDebug())
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
    }

    public function pdo()
    {
        return $this->db->getPdo();
    }

    public function query($statement)
    {
        return $this->pdo()->prepare($statement);
    }

    public function delete($table, $primaryKey = null)
    {
        return $this->db->delete($table, $primaryKey);
    }

    public function insert($table, $values = [])
    {
        return $this->db->insertInto($table, $values);
    }

    public function update($table, $set = [], $primaryKey = null)
    {
        return $this->db->update($table, $set, $primaryKey);
    }

    public function from($table, $primaryKey = null)
    {
        return $this->db->from($table, $primaryKey);
    }

}