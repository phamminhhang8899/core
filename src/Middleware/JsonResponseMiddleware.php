<?php

namespace Favez\Mvc\Middleware;

use Favez\Mvc\Http\Response\Json;
use Slim\Http\Request;
use Slim\Http\Response;

class JsonResponseMiddleware
{
    
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $response = $next($request, $response);
        
        if ($response instanceof Json)
        {
            return $response->send();
        }
        else if(is_array($response))
        {
            return (new Json())
                ->assign($response)
                ->send();
        }
        else if(is_object($response) && !($response instanceof Response))
        {
            return (new Json())
                ->assign($response)
                ->send();
        }
        else if(!($response instanceof Response))
        {
            return (new Json())
                ->assign(gettype($response), $response)
                ->send();
        }
        
        return $response;
    }

}