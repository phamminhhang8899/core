<?php

namespace Favez\Mvc\Subscriber;

use Favez\Mvc\App;
use Favez\Mvc\Exception\InvalidSubscriberException;

class Manager
{

    /**
     * @var App
     */
    protected $app = null;
    
    protected $registered = false;

    public function __construct(App $app)
    {
        $this->app        = $app;
        $this->registered = false;
    }

    public function initialize(Subscriber $subscriber)
    {
        foreach ($subscriber->getSubscribedEvents() as $name => $callable)
        {
            App::events()->subscribe($name, $callable);
        }
    }
    
    public function register()
    {
        if ($this->isRegistered())
        {
            throw new \Exception('Can not execute SubscriberManager::register twice.');
        }
        
        $subscribers = $this->app->config('subscribers', []);
    
        foreach ($subscribers as $subscriber)
        {
            if (!($subscriber instanceof \Favez\Mvc\Subscriber\Subscriber))
            {
                throw new InvalidSubscriberException();
            }
        
            $this->initialize($subscriber);
        }
        
        $this->setRegistered(true);
    }
    
    public function isRegistered()
    {
        return $this->registered;
    }
    
    public function setRegistered($registered)
    {
        $this->registered = (bool) $registered;
    }

}