<?php

namespace Favez\Mvc;

class Container implements \ArrayAccess
{

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function get($key, $default = null)
    {
        if (isset($this->data[$key]))
        {
            return $this->data[$key];
        }

        return $default;
    }

    public function has($key)
    {
        return isset($this->data[$key]);
    }

    public function reset($key)
    {
        if (isset($this->data[$key]))
        {
            unset($this->data[$key]);
        }
    }

    public function __get($key)
    {
        return $this->get($key);
    }

    public function __set($key, $value)
    {
        $this->set($key, $value);
    }

    public function offsetExists($key)
    {
        return $this->has($key);
    }

    public function offsetSet($key, $value)
    {
        $this->set($key, $value);
    }

    public function offsetGet($key)
    {
        return $this->get($key);
    }

    public function offsetUnset($key)
    {
        $this->reset($key);
    }

}