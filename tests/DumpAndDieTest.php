<?php

use PHPUnit\Framework\TestCase;
use Slim\Http\Body;

class DumpAndDieTest extends TestCase
{
    
    public function testDD()
    {
        $this->assertEquals('{"string":"hello world"}', dd('hello world', true)->getBody()->__toString());
        
        $this->assertEquals('{"boolean":true}', dd(true, true)->getBody()->__toString());

        $this->assertEquals('{"integer":1}', dd(1, true)->getBody()->__toString());
        
        $this->assertEquals('{"double":2.1}', dd(2.1, true)->getBody()->__toString());
    }
    
}