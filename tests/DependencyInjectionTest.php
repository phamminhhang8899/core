<?php

use \PHPUnit\Framework\TestCase;
use \Favez\Mvc\App;

class DependencyInjectionTest extends TestCase
{
        
    public function testInjection()
    {
        $this->assertTrue(gettype(App::path()) == 'string');
        $this->assertTrue(App::app() instanceof \Favez\Mvc\App);
        $this->assertTrue(App::cache() instanceof \Stash\Pool);
        $this->assertTrue(App::cookies() instanceof \Favez\Mvc\Http\Cookies);
        //$this->assertTrue($modules->db() instanceof \Favez\Mvc\Database\Database);
        $this->assertTrue(App::dispatcher() instanceof \Favez\Mvc\Dispatcher);
        $this->assertTrue(App::events() instanceof \Favez\Mvc\Event\Manager);
        $this->assertTrue(App::subscriber() instanceof \Favez\Mvc\Subscriber\Manager);
        //$this->assertTrue($modules->models() instanceof \Favez\ORM\EntityManager);
        $this->assertTrue(App::json() instanceof \Favez\Mvc\Http\Response\Json);
        $this->assertTrue(App::request() instanceof \Slim\Http\Request);
        $this->assertTrue(App::response() instanceof \Slim\Http\Response);
        $this->assertTrue(App::session() instanceof \SlimSession\Helper);
        $this->assertTrue(App::view() instanceof \Favez\Mvc\View\View);

        $this->assertTrue(\Favez\Mvc\App::di() instanceof \Favez\Mvc\DI\Container);
    }
    
}
