<?php

require_once __DIR__ . '/../vendor/autoload.php';

\Favez\Mvc\App::instance([
    'config' => [
        'modules' => [
            'frontend' => [
                'controller' => [
                    'namespace'     => 'App\\Controllers\\',
                    'class_suffix'  => 'Controller',
                    'method_suffix' => 'Action'
                ]
            ]
        ],
        'view' => [
            'theme_path' => 'themes/',
            'cache_path' => 'cache/twig/'
        ],
        'database' => [
            'host' => 'localhost',
            'shem' => 'test',
            'user' => 'root',
            'pass' => 'vagrant'
        ],
        'app' => [
            'path'       => __DIR__ . '/',
            'cache_path' => 'cache/'
        ],
        'subscriber' => [
        
        ],
        'debug' => true,
        'test'  => true
    ],
    'settings' => [
        'displayErrorDetails' => true
    ]
]);